//
//  MovieCollectionViewCell.swift
//  Top Ranked Movies
//
//  Created by Oscar Sevilla Garduño on 01/12/21.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var movieImageView: UIImageView!
    @IBOutlet var movieTitle: UILabel!
    
    
}
