//
//  Movies.swift
//  TopRankedMovies
//
//  Created by Oscar Sevilla Garduño on 30/11/21.
//

import Foundation

struct Movie: Codable {
    var id: Int
    var title: String
    var release_date: String
    var overview: String
    var poster_path: String
    var vote_average: Double
    var original_title: String?
}
