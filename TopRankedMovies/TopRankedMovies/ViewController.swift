//
//  ViewController.swift
//  TopRankedMovies
//
//  Created by Oscar Sevilla Garduño on 26/11/21.
//

import UIKit

class ViewController: UIViewController {
    
    var movies = [Movie]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        if checkData() {
            self.getData { (status) in
                if status {
                    self.saveData()
                }
            }
        }
        self.navigationController?.pushViewController(TopMoviesViewController(movies: self.movies), animated: false)
    }
    
    @objc func getData(completion: @escaping (Bool) -> () ){
        DispatchQueue.main.async {
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=ead3dffc223ccd9589813be58467ac31&language=en-US&page=1")!) { [self]
                    data, response, error in
                    
                    guard error == nil else { return }
                    guard let content = data else { return }
                    guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                        print("Not containing JSON")
                        return
                    }
                    
                    var index = 0
                    if let movieDicts = json["results"] as? [[String: Any]] {
                        for data in movieDicts {
                            if let id = data["id"] as? Int, let title = data["title"] as? String, let release = data["release_date"] as? String, let overview = data["overview"] as? String, let poster = data["poster_path"] as? String, let vote_average = data["popularity"] as? Double {
                                self.movies.append(Movie(id: id,
                                                         title: title,
                                                         release_date: release,
                                                         overview: overview,
                                                         poster_path: poster,
                                                         vote_average: vote_average))
                                index += 1
                            }

                            if index >= 10{
                                completion(true)
                                return
                            }
                        }
                    }
            }
            task.resume()
        }
    }
    
    func checkData() -> Bool {
        getDate()
        if let data = UserDefaults.standard.value(forKey: "movies") as? Data {
            let moviesT = try? PropertyListDecoder().decode(Array<Movie>.self, from: data)
            if moviesT?.count ?? 0 > 1 {
                self.movies = moviesT!
//                print(self.movies)
                return false
            }
        }
        return true
    }
    
    func saveData(){
        let defaults = UserDefaults.standard
        defaults.set(try? PropertyListEncoder().encode(self.movies), forKey: "movies")
        defaults.synchronize()
    }
    
    func getDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        print("La fecha es: \(dateInFormat)")
    }
}
