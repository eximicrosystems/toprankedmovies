//
//  MovieInfoViewController.swift
//  TopRankedMovies
//
//  Created by Oscar Sevilla Garduño on 02/12/21.
//

import UIKit

class MovieInfoViewController: UIViewController {

    @IBOutlet var poster: UIImageView!
    @IBOutlet var movieTitle: UILabel!
    @IBOutlet var overview: UILabel!
    @IBOutlet var releaseDate: UILabel!
    @IBOutlet var originalTitle: UILabel!
    
    let posterPath: String = "https://www.themoviedb.org/t/p/w440_and_h660_face"
    
    var movie: Movie!
    
    init(movie: Movie) {
        self.movie = movie
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    func loadData(){
        poster.downloaded(from: "\(posterPath)\(movie.poster_path)")
        movieTitle.text = movie.title
        overview.text = movie.overview
        releaseDate.text = movie.release_date
        originalTitle.text = "\(movie.vote_average)"
    }
    
}
